package ru.bokhan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.repository.IProjectRepository;
import ru.bokhan.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    @NotNull
    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(@NotNull final Project project) {
        projects.add(project);
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public void addAll(@NotNull final List<Project> projectList) {
        projects.addAll(projectList);
    }

    @Override
    public void load(@NotNull final List<Project> projectList) {
        clear();
        addAll(projectList);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Project project) {
        if (!userId.equals(project.getUserId())) return;
        projects.remove(project);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            if (userId.equals(project.getUserId())) result.add((project));
        }
        return result;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<Project> userProjects = findAll(userId);
        projects.removeAll(userProjects);
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Nullable
    @Override
    public Project findById(@NotNull final String userId, @NotNull final String id) {
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            if (id.equals((project.getId())) && userId.equals(project.getUserId()))
                return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return findAll(userId).get(index);
    }

    @Nullable
    @Override
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            if (name.equals((project.getName())) && userId.equals(project.getUserId()))
                return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findById(userId, id);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Nullable
    @Override
    public Project removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Override
    @Nullable
    public Project removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findByName(userId, name);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

}
