package ru.bokhan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.repository.IUserRepository;
import ru.bokhan.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository implements IUserRepository {

    @NotNull
    private final List<User> users = new ArrayList<>();

    @NotNull
    @Override
    public List<User> findAll() {
        return users;
    }

    @NotNull
    @Override
    public User add(@NotNull final User user) {
        users.add(user);
        return user;
    }

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        for (@Nullable final User user : users) {
            if (user == null) continue;
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        for (@Nullable final User user : users) {
            if (user == null) continue;
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public void addAll(@NotNull final List<User> userList) {
        users.addAll(userList);
    }

    @Override
    public void load(@NotNull final List<User> userList) {
        clear();
        addAll(userList);
    }

    @Override
    public void clear() {
        users.clear();
    }

    @Nullable
    @Override
    public User remove(@NotNull final User user) {
        @Nullable final User found = findById(user.getId());
        if (found == null) return null;
        users.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeById(@NotNull final String id) {
        @Nullable final User user = findById(id);
        if (user == null) return null;
        return remove(user);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

}