package ru.bokhan.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.service.IDomainService;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.constant.DataConstant;
import ru.bokhan.tm.dto.Domain;
import ru.bokhan.tm.enumerated.Role;

import java.io.FileInputStream;

public final class DataJsonLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-load";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from json file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON LOAD]");
        @NotNull final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_JSON);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final Domain domain = mapper.readValue(fileInputStream, Domain.class);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        fileInputStream.close();
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
