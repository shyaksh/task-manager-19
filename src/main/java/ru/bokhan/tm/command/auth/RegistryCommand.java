package ru.bokhan.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.util.TerminalUtil;

public final class RegistryCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "registry";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Registry";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("[ENTER LOGIN:]");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER E-MAIL:]");
        @NotNull final String email = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
        System.out.println("[OK:]");
    }

}

