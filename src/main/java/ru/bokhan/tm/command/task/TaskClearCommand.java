package ru.bokhan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;

public final class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CLEAR TASKS]");
        serviceLocator.getTaskService().clear(userId);
        System.out.println("[OK]");
    }

}
