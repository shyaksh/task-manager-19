package ru.bokhan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.entity.Task;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[LIST TASKS]");
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        for (@Nullable final Task task : tasks) System.out.println(task);
        System.out.println("[OK]");
    }

}
