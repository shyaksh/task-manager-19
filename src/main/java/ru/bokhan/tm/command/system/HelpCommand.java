package ru.bokhan.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.bootstrap.Bootstrap;
import ru.bokhan.tm.command.AbstractCommand;

import java.util.Collection;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String argument() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Bootstrap bootstrap = (Bootstrap) serviceLocator;
        @NotNull final Collection<AbstractCommand> commands = bootstrap.getCommands();
        for (@Nullable AbstractCommand command : commands) {
            if (command == null) continue;
            System.out.printf("%-25s| %s\n", command.name(), command.description());
        }
        System.out.println("[OK]");
    }

}
