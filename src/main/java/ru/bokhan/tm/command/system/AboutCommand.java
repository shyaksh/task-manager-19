package ru.bokhan.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String argument() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Pavel Bokhan");
        System.out.println("E-MAIL: shyaksh@gmail.com");
    }

}
