package ru.bokhan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.entity.Project;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[LIST PROJECTS]");
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll(userId);
        for (@Nullable final Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

}
