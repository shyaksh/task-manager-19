package ru.bokhan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    @NotNull
    List<User> findAll();

    @NotNull
    User add(@NotNull User user);

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    void addAll(@NotNull List<User> userList);

    void load(@NotNull List<User> userList);

    void clear();

    @Nullable
    User remove(@NotNull User user);

    @Nullable
    User removeById(@NotNull String id);

    @Nullable
    User removeByLogin(@NotNull String login);

}
