package ru.bokhan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.enumerated.Role;

public interface IAuthService {

    @NotNull
    String getUserId();

    void login(@Nullable String login, @Nullable String password);

    void checkRoles(@Nullable Role[] roles);

    boolean isAuth();

    void logout();

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
